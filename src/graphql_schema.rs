extern crate dotenv;

use crate::resolvers::user_resolvers::{create_user_resolver, user_resolver, users_resolver, update_user_resolver};
use crate::typedefs::user_typedefs::{User, UserInput, UserWhereInput, UsersWhereInput, UserUpdateInput};
use juniper::{FieldResult, RootNode};
use r2d2::Pool;
use r2d2_postgres::PostgresConnectionManager;

#[derive(Clone)]
pub struct Context {
    pub db: Pool<PostgresConnectionManager>,
}

impl juniper::Context for Context {}

pub struct QueryRoot {}

graphql_object!(QueryRoot: Context |&self| {

    field users(&executor, where_input: UsersWhereInput) -> FieldResult<Vec<User>> {
        users_resolver(executor.context(), where_input)
    }

    field user(&executor, where_input: UserWhereInput) -> FieldResult<Option<User>> {
       user_resolver(executor.context(), where_input)
    }
});

pub struct MutationRoot;

graphql_object!(MutationRoot: Context |&self| {

    field create_user(&executor, input: UserInput) -> FieldResult<Option<User>> {
        create_user_resolver(executor.context(), input)
    }

    field update_user(&executor, where_input: UserWhereInput, input: UserUpdateInput) -> FieldResult<Option<User>> {
        update_user_resolver(executor.context(), where_input, input)
    }
});

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}
