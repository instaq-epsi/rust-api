use r2d2_postgres::{ PostgresConnectionManager, TlsMode::None as NoTls};
use r2d2::Pool;


pub fn init_database(db_url: String) -> Pool<PostgresConnectionManager> {
    let manager = PostgresConnectionManager::new(
        db_url,
        NoTls,
    ).unwrap();
    Pool::new(manager).unwrap()
}