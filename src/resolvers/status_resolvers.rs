use crate::graphql_schema::Context;
use crate::typedefs::status_typedefs::Status;
use crate::utils::db::query_row;
use juniper::FieldError;
use postgres::rows::Row;

pub fn status_from_sql(row: Row) -> Option<Status> {
    Some(Status {
        id: row.get(0),
        status_name: row.get(1),
    })
}

pub fn status_resolver(
    context: &Context,
    where_status: String,
) -> Result<Option<Status>, FieldError> {
    let sql = "SELECT status_name FROM status WHERE status_name = &";
    match query_row(context, &sql, &[&where_status], status_from_sql) {
        Ok(Some(id)) => Ok(Some(id)),
        Ok(None) => Err(FieldError::new(
            "Nothing was returned",
            graphql_value!({"Error returning status": "Nothing was returned"}),
        )),
        Err(e) => Err(FieldError::from(e)),
    }
}
