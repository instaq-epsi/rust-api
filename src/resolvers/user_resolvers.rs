use crate::graphql_schema::Context;
use crate::typedefs::user_typedefs::{
    User, UserInput, UserUpdateInput, UserWhereInput, UsersWhereInput,
};
use crate::utils::db::{exec, query_row, query_rows};
use juniper::FieldError;
use postgres::rows::Row;
use uuid::Uuid;

fn user_from_sql(row: Row) -> Option<User> {
    Some(User {
        id: row.get(0),
        user_id: row.get(1),
        user_name: row.get(2),
        firstname: row.get(3),
        lastname: row.get(4),
        description: row.get(5),
        avatar_url: row.get(6),
        user_status: row.get(7),
    })
}

pub fn user_resolver(
    context: &Context,
    where_input: UserWhereInput,
) -> Result<Option<User>, FieldError> {
    match where_input {
        UserWhereInput {
            id,
            user_id,
            user_name,
        } => match (id, user_id, user_name) {
            (Some(id), ..) => {
                let result = get_user_by_id(context, id);
                match result {
                    Ok(user_result) => Ok(user_result),
                    Err(e) => Err(e),
                }
            }
            (_, Some(user_id), ..) => {
                let result = get_user_by_user_id(context, user_id);
                match result {
                    Ok(user_result) => Ok(user_result),
                    Err(e) => Err(e),
                }
            }
            (_, _, Some(user_name)) => {
                let result = get_user_by_user_name(context, user_name);
                match result {
                    Ok(user_result) => Ok(user_result),
                    Err(e) => Err(e),
                }
            }
            (None, None, None) => Err(FieldError::new(
                "Please don't provide an empty search object",
                graphql_value!({ "bad_request": "Please don't provide an empty search object" }),
            )),
        },
    }
}

fn get_user_by_id(context: &Context, id_param: Uuid) -> Result<Option<User>, FieldError> {
    let query_result = query_row(context, "SELECT \"user\".id, user_id, user_name, firstname, lastname, description, avatar_url, status.status_name as user_status
     FROM \"user\" JOIN status ON \"user\".user_status = status.id WHERE \"user\".id=$1;", &[&id_param], user_from_sql);
    match query_result {
        Ok(Some(row)) => Ok(Some(row)),
        Ok(None) => Ok(None),
        Err(e) => Err(FieldError::from(e)),
    }
}

fn get_user_by_user_id(
    context: &Context,
    user_id_param: String,
) -> Result<Option<User>, FieldError> {
    let query_result = query_row(context, "SELECT \"user\".id, user_id, user_name, firstname, lastname, description, avatar_url, status.status_name as user_status
     FROM \"user\" JOIN status ON \"user\".user_status = status.id WHERE user_id=$1;", &[&user_id_param], user_from_sql);
    match query_result {
        Ok(Some(row)) => Ok(Some(row)),
        Ok(None) => Ok(None),
        Err(e) => Err(FieldError::from(e)),
    }
}

fn get_user_by_user_name(
    context: &Context,
    user_name_param: String,
) -> Result<Option<User>, FieldError> {
    let query_result = query_row(context, "SELECT \"user\".id, user_id, user_name, firstname, lastname, description, avatar_url, status.status_name as user_status
     FROM \"user\" JOIN status ON \"user\".user_status = status.id WHERE user_name = $1;", &[&user_name_param], user_from_sql);
    match query_result {
        Ok(Some(row)) => Ok(Some(row)),
        Ok(None) => Ok(None),
        Err(e) => Err(FieldError::from(e)),
    }
}

pub fn users_resolver(
    context: &Context,
    where_input: UsersWhereInput,
) -> Result<Vec<User>, FieldError> {
    let query = build_users_query(&where_input);
    let query_result = match where_input {
        UsersWhereInput {
            user_name,
            user_status,
        } => match (user_name, user_status) {
            (user_name, Some(user_status)) => {
                let user_name_param = format!("%{}%", user_name);
                query_rows(
                    context,
                    &query,
                    &[&user_name_param, &user_status],
                    user_from_sql,
                )
            }
            (user_name, None) => {
                let user_name_param = format!("%{}%", user_name);
                query_rows(context, &query, &[&user_name_param], user_from_sql)
            }
        },
    };
    match query_result {
        Ok(rows) => Ok(rows),
        Err(e) => Err(FieldError::from(e)),
    }
}

fn build_users_query(where_input: &UsersWhereInput) -> String {
    let condition = match where_input {
        UsersWhereInput {
            user_name,
            user_status,
        } => match (user_name, user_status) {
            (_user_name, Some(_user_status)) => {
                format!("WHERE user_name LIKE $1 AND \"user\".user_status = $2;")
            }
            (_user_name, None) => format!("WHERE user_name LIKE $1;"),
        },
    };
    format!("SELECT \"user\".id, user_id, user_name, firstname, lastname, description, avatar_url, status.status_name as user_status
     FROM \"user\" JOIN status ON \"user\".user_status = status.id {};", condition)
}

pub fn create_user_resolver(
    context: &Context,
    data: UserInput,
) -> Result<Option<User>, FieldError> {
    let insert_result = exec(context,"INSERT INTO \"user\" (user_id, user_name, firstname, lastname, description, avatar_url) VALUES ($1, $2, $3, $4, $5, $6);", &[&data.user_id, &data.user_name, &data.firstname, &data.lastname, &data.description, &data.avatar_url]);
    match insert_result {
        Ok(_) => get_user_by_user_name(context, data.user_name),
        Err(e) => Err(FieldError::from(e)),
    }
}

pub fn update_user_resolver(
    context: &Context,
    where_input: UserWhereInput,
    input: UserUpdateInput,
) -> Result<Option<User>, FieldError> {
    let cloned_where_input = where_input.clone();
    let sql_input = match user_update_input_to_sql(input) {
        Ok(sql) => sql,
        Err(e) => return Err(FieldError::from(e)),
    };
    let sql_where_input = match user_where_input_to_sql(where_input) {
        Ok(sql) => sql,
        Err(e) => return Err(FieldError::from(e)),
    };
    let sql = format!(
        "UPDATE \"user\" SET {} WHERE {}",
        sql_input, sql_where_input
    );
    println!("sql: {}", sql);
    match exec(context, &sql, &[]) {
        Ok(_) => (),
        Err(e) => return Err(FieldError::from(e)),
    };
    user_resolver(context, cloned_where_input)
}

pub fn user_where_input_to_sql(where_input: UserWhereInput) -> Result<String, FieldError> {
    let mut sql_list = Vec::new();
    where_input.id.as_ref().and_then(|v| {
        sql_list.push(format!("id = '{}'", v));
        Some(v)
    });
    where_input.user_id.as_ref().and_then(|v| {
        sql_list.push(format!("user_id = '{}'", v));
        Some(v)
    });
    where_input.user_name.as_ref().and_then(|v| {
        sql_list.push(format!("user_name LIKE '{}'", v));
        Some(v)
    });
    if sql_list.len() <= 0 {
        return Err(FieldError::new(
            "Missing where input argument",
            graphql_value!({"Argument error": "Missing where input argument"}),
        ));
    }
    Ok(sql_list.join(", "))
}

pub fn user_update_input_to_sql(input: UserUpdateInput) -> Result<String, FieldError> {
    let mut sql_list = Vec::new();
    input.user_name.as_ref().and_then(|v| {
        sql_list.push(format!("user_name = '{}'", v));
        Some(v)
    });
    input.firstname.as_ref().and_then(|v| {
        sql_list.push(format!("firstname = '{}'", v));
        Some(v)
    });
    input.lastname.as_ref().and_then(|v| {
        sql_list.push(format!("lastname = '{}'", v));
        Some(v)
    });
    input.description.as_ref().and_then(|v| {
        sql_list.push(format!("description = '{}'", v));
        Some(v)
    });
    input.avatar_url.as_ref().and_then(|v| {
        sql_list.push(format!("avatar_url = '{}'", v));
        Some(v)
    });
    input.user_status.as_ref().and_then(|v| {
        sql_list.push(format!(
            "user_status = (SELECT id FROM status WHERE status_name LIKE '{}')",
            v
        ));
        Some(v)
    });
    if sql_list.len() <= 0 {
        return Err(FieldError::new(
            "Missing input argument",
            graphql_value!({"Argument error": "Missing input argument"}),
        ));
    }
    Ok(sql_list.join(", "))
}
