extern crate dotenv;
extern crate iq_api;

use iq_api::db::init_database;
use iq_api::graphql_schema::Context;
use iq_api::resolvers::user_resolvers::{
    create_user_resolver, update_user_resolver, user_resolver, users_resolver,
};
use iq_api::typedefs::user_typedefs::{
    UserInput, UserUpdateInput, UserWhereInput, UsersWhereInput, User
};

use std::env;

fn init_context() -> Context {
    dotenv::dotenv().ok();
    let db_url = env::var("DATABASE_URL_TESTS").expect("DATABASE_URL must be set");
    let pool = init_database(db_url);
    Context { db: pool.clone() }
}

#[test]
fn test_user() {
    let user_result = user_resolver(
        &init_context(),
        UserWhereInput {
            id: None,
            user_id: None,
            user_name: Some("Kedur".to_owned()),
        },
    );
    let user = match user_result {
        Ok(Some(user)) => user,
        Ok(None) => panic!("Option returned None"),
        Err(e) => panic!(e),
    };
    assert_eq!(user.user_name, "Kedur");
}

#[test]
fn test_users() {
    let users_result = users_resolver(
        &init_context(),
        UsersWhereInput {
            user_name: "ur".to_owned(),
            user_status: None,
        },
    );
    let users = match users_result {
        Ok(users) => users,
        Err(e) => panic!(e),
    };
    assert_eq!(users.len(), 5);
}

#[test]
fn test_create_user() {
    let user_creation_result = create_user_resolver(
        &init_context(),
        UserInput {
            user_id: "qpsdkaozkdaofgqfjs".to_owned(),
            user_name: "test".to_owned(),
            firstname: "".to_owned(),
            lastname: "".to_owned(),
            description: "".to_owned(),
            avatar_url: "String".to_owned(),
        },
    );
    let user_created = match user_creation_result {
        Ok(Some(user)) => user,
        Ok(None) => panic!("User creation returned None"),
        Err(e) => panic!(e),
    };
    assert_eq!(user_created.user_name, "test".to_owned());
}

#[test]
fn test_update_user() {
    let user_creation_result = user_resolver(
        &init_context(),
        UserWhereInput {
            id: None,
            user_id: None,
            user_name: Some("Kedur".to_owned()),
        },
    );
    let created_user = match user_creation_result {
        Ok(Some(user)) => user,
        Ok(None) => panic!("User creation returned None"),
        Err(e) => panic!(e),
    };
    let update_input = UserUpdateInput {
        description: Some("updated description".to_owned()),
        firstname: Some("updated firstname".to_owned()),
        lastname: Some("updated lastname".to_owned()),
        user_name: Some("updated username".to_owned()),
        avatar_url: Some("updated avatar_url".to_owned()),
        user_status: Some("MODERATED".to_owned()),
    };
    let updated_user = User {
        id: created_user.id,
        user_id: created_user.user_id,
        description: "updated description".to_owned(),
        firstname: "updated firstname".to_owned(),
        lastname: "updated lastname".to_owned(),
        user_name: "updated username".to_owned(),
        avatar_url: "updated avatar_url".to_owned(),
        user_status: "MODERATED".to_owned(),
    };
    let user_where_input = UserWhereInput {
        id: Some(created_user.id),
        user_id: None,
        user_name: None,
    };
    let updated_user_result = update_user_resolver(&init_context(), user_where_input, update_input);
    match updated_user_result {
        Ok(Some(user)) => assert_eq!(user, updated_user),
        Ok(None) => panic!("User updated returned None"),
        Err(e) => {
            println!("Error {}", e.message());
            panic!(e)
        }
    };
}
