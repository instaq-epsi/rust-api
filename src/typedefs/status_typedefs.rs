use uuid::Uuid;

pub struct Status {
    pub id: Uuid,
    pub status_name: String,
}