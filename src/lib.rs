extern crate dotenv;
#[macro_use]
extern crate juniper;

use std::env;
use std::io;
use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{middleware::Logger, web, App, Error, HttpResponse, HttpServer};
use dotenv::dotenv;
use futures::future::Future;
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;

pub mod db;
pub mod graphql_schema;
pub mod resolvers;
pub mod typedefs;
pub mod utils;

use crate::db::init_database;
use crate::graphql_schema::{create_schema, Context, Schema};

fn graphiql() -> HttpResponse {
    let graphql_url = env::var("GRAPHQL_URL").expect("GRAPHQL_URL must be set");
    let html = graphiql_source(&graphql_url);
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

fn graphql(
    st: web::Data<Arc<Schema>>,
    ctx: web::Data<Context>,
    data: web::Json<GraphQLRequest>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        let res = data.execute(&st, &ctx);
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .map_err(Error::from)
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}

pub fn init_server() -> io::Result<()> {
    dotenv().ok();
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let port = env::var("PORT").expect("PORT must be set");
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = init_database(db_url);
    let schema_context = Context { db: pool.clone() };
    let schema = std::sync::Arc::new(create_schema());
    HttpServer::new(move || {
        App::new()
            .wrap(
                Cors::new().send_wildcard(),
            )
            .wrap(Logger::default())
            .data(schema.clone())
            .data(schema_context.clone())
            .service(web::resource("/graphql").route(web::post().to_async(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
}
