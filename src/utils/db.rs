use crate::graphql_schema::Context;
use postgres::rows::Row;
use postgres::types::ToSql;
use postgres::Error;

pub fn exec(context: &Context, sql: &str, params: &[&dyn ToSql]) -> Result<(), Error> {
    let connection = context.db.get().unwrap();
    let statement = connection.prepare(sql)?;
    let result = statement.execute(params);
    match result {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}

pub fn query_row<F, Object>(
    context: &Context,
    sql: &str,
    params: &[&dyn ToSql],
    object_from_sql: F,
) -> Result<Option<Object>, Error>
where
    F: Fn(Row) -> Option<Object>,
{
    let connection = context.db.get().unwrap();
    let statement = connection.prepare(sql)?;
    let rows_result = statement.query(params);
    match rows_result {
        Ok(rows) => {
            if rows.len() > 0 {
                Ok(object_from_sql(rows.get(0)))
            } else {
                Ok(None)
            }
        }
        Err(e) => Err(e),
    }
}

pub fn query_rows<F, Object>(
    context: &Context,
    sql: &str,
    params: &[&dyn ToSql],
    object_from_sql: F,
) -> Result<Vec<Object>, Error>
where
    F: Fn(Row) -> Option<Object>,
{
    let connection = context.db.get().unwrap();
    let statement = connection.prepare(sql)?;
    let rows_result = statement.query(params);
    match rows_result {
        Ok(rows) => {
            if rows.len() > 0 {
                let mut results = Vec::new();
                for row in rows.iter() {
                    match object_from_sql(row) {
                        Some(r) => results.push(r),
                        None => (),
                    }
                }
                Ok(results)
            } else {
                Ok(Vec::new())
            }
        }
        Err(e) => Err(e),
    }
}
