use std::io::Result;

use iq_api::init_server;

fn main() -> Result<()> {
    init_server()
}
