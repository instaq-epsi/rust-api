use uuid::Uuid;
use std::fmt::Debug;

#[derive(GraphQLObject, PartialEq, Debug)]
#[graphql(description = "A User of instaq")]
pub struct User {
    pub id: Uuid,
    pub user_id: String,
    pub user_name: String,
    pub firstname: String,
    pub lastname: String,
    pub description: String,
    pub avatar_url: String,
    pub user_status: String,
}

#[derive(juniper::GraphQLInputObject)]
pub struct UserInput {
    pub user_id: String,
    pub user_name: String,
    pub firstname: String,
    pub lastname: String,
    pub description: String,
    pub avatar_url: String,
}

#[derive(juniper::GraphQLInputObject)]
pub struct UserUpdateInput {
    pub user_name: Option<String>,
    pub firstname: Option<String>,
    pub lastname: Option<String>,
    pub description: Option<String>,
    pub avatar_url: Option<String>,
    pub user_status: Option<String>,
}

#[derive(juniper::GraphQLInputObject, Clone)]
pub struct UserWhereInput {
    pub id: Option<Uuid>,
    pub user_id: Option<String>,
    pub user_name: Option<String>,
}

#[derive(juniper::GraphQLInputObject)]
pub struct UsersWhereInput {
    pub user_name: String,
    pub user_status: Option<String>,
}
